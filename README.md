# alacritty config
Configuration for fast terminal emulator called alacritty.

## screenshot
![](/screenshot/alacritty-debian.png)

## alacritty config location (linux)
```
~/.config/alacritty/alacritty.yml
```

## alacritty desktop file location
```
$HOME/.local/share/applications
```
``
$HOME=/home/user
``

### Terminal Theme
***iTerm 2***\
https://iterm2colorschemes.com/
